
'''
    Name:           restaurant_status.py
    Description:    A python script to search all open restaurant
    input:          .csv file containing the details restaurant data
    output:         '/n' delimited string
    usage:          python restaurant_status.py
                            <filename:>
                            <date:YYYY-DD-MM>
                            <time:HH-MM am/pm>
    example:        python restaurant_status.py
                            restaurant.csv
                            2018-07-12
                            05-30 pm
'''

import csv
from datetime import datetime

#function to convert date and time into key value format
def convert_date_time(date_time):
    timeDetails = {}
    days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
    date_time = date_time.split('/')
    for index in range(len(date_time)):
        date_time[index] = date_time[index].strip()
        date_time[index] = date_time[index].replace(', ',',')
        date_time[index] = date_time[index].replace(' - ','-')
        date_time[index] = date_time[index].replace(' am','am')
        date_time[index] = date_time[index].replace(' pm','pm')
        date_time[index] = date_time[index].replace(' ',',')
        dateTimeSplit = date_time[index].split(',')

        length = len(dateTimeSplit)

        timep = dateTimeSplit[-1]
        timep = timep.split('-')

        for index in range(len(timep)):
            if len(timep[index]) > 4:
                v = timep[index].split(':')
                if timep[index][-2] == 'p':
                    timep[index] = str(int(v[0]) + 12) + str(int(v[1][:-2]))
                else:
                    timep[index] = str(int(v[0])) + str(int(v[1][:-2]))
            else:
                if timep[index][-2] == 'p':
                    timep[index] = str(int(timep[index][:-2]) + 12)
                else:
                    timep[index] = str(int(timep[index][:-2]))
        dateTimeSplit[length-1] = timep[0]+":"+timep[1]

        for index in range(length - 1):
            if len(dateTimeSplit[index]) > 3:
                start = days.index(dateTimeSplit[index][:3])
                end = days.index(dateTimeSplit[index][4:])
                for dayIndex in range(start,end + 1):
                    timeDetails[days[dayIndex]] = dateTimeSplit[length - 1]
            else:
                timeDetails[dateTimeSplit[index]] = dateTimeSplit[length - 1]

    date_time = timeDetails
    timeDetails = {}

    return date_time

#function to read the data from file
def fileOpen(filename):
    res = {}
    try:
        with open(filename) as csv_file:
            csv_data = csv.reader(csv_file,delimiter = ',')
            for line in csv_data:
                res[line[0]] = convert_date_time(line[1])
    except FileNotFoundError:
        print('File does not exist')

    return res

#function for parse and convert the input time
def parseTime(given_time):
    if len(given_time) > 5:
        given_time = given_time.split(':')
        if given_time[1][-2] == 'p':
            given_time = int(given_time[0]) + 12 + int(given_time[1][:-3])
        else:
            given_time = int(given_time[0]) + int(given_time[1][:-3])
    else:
        if given_time[-2] == 'p':
            given_time = int(given_time[:-3]) + 12
        else:
            given_time = int(given_time[:-3])
    return given_time

#search function for the available restaurant
def find_restaurant(filename,day_time):
    names = []
    res = fileOpen(filename)
    day,time = map(str,day_time.split(','))
    time = parseTime(time)

    for i in res:
        if day in res[i]:
            starttime,endtime = map(int,res[i][day].split(':'))
            if starttime > endtime:
                if time >= starttime or time < endtime:
                    names.append(i)
            if starttime < endtime:
                if time >= starttime and time < endtime:
                    names.append(i)
    return names

#main function to take input and printing search result
def main():

    days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']

    while True:
        filename = input('Enter the file name to search: ')
        filename_ext = filename.split('.')
        try:
            if filename_ext[1] == 'csv':
                print('Input filename is csv file')
                break
            else:
                print('Input filename is not csv file')
        except IndexError:
            print('Enter a csv file')

    date = input('Enter date in in the format of YYYY-DD-MM: ')
    time = input('Enter time in the format of HH:MM am/pm: ')

    date = datetime.strptime(date,'%Y-%d-%m').date()
    day = days[date.weekday()]
    day_time = day +','+ time
    result = find_restaurant(filename,day_time)
    noOfRestaurant = 0
    for i in result:
        noOfRestaurant += 1
        print(str(noOfRestaurant) + " : " + i)

    print(f'{noOfRestaurant} restaurant is open at the time of {time}')


if __name__ == '__main__':
    main()
